import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  html {
    color: #1b1b1b;
  }

  body {
    margin: 0;
    overflow-x: hidden;
    box-sizing: border-box;
    font-size: 1.125rem;
    line-height: 1.6;
    font-family: Arial;
    -moz-osx-font-smoothing: grayscale;
    text-rendering: optimizeSpeed;
  }

  /** Interaction **/

  ::selection {
    background: tomato;
    color: white;
  }

  a {
    color: rebeccapurple;
  }

  a:hover, a:focus {
    color: tomato;
    text-decoration: none;
  }

  a:active {
    background-color: tomato;
    color: white;
  }

  h1, h2, h3, h4, h5, h6 {
    a:link, a:visited {
      display: inline-block;
      color: inherit;
      text-decoration: none;
    }

    a:hover, a:focus {
      color: inherit;
      text-decoration: underline;
    }

    a:active {
      background-color: inherit;
      color: inherit;
    }
  }

  button {
    font: inherit;
    cursor: pointer;
  }

  label {
    display: block;
  }

  input, textarea {
    display: block;
    font: inherit;
    border-radius: .25rem;
    border: 1px solid silver;
    padding: calc(0.5rem - 1px) 1rem;
    width: 100%;
    box-sizing: border-box;
    outline: none;
    line-height: 1.75rem;

    &:focus {
      padding: calc(0.5rem - 2px) calc(1rem - 1px);
      border: 2px solid rebeccapurple;
    }
  }

  textarea {
    min-height: 2.75rem;
    height: calc(2.75rem * 10);
    max-height: calc(2.75rem * 16);
    resize: vertical;
  }

  /** Layout **/

  ul, ol {
    margin: 1rem 0;
    padding-left: 2rem;
  }

  hr {
    border: 0;
    border-top: 1px solid silver;
    margin: 1rem 0;
  }

  /** Typography **/

  p {
    white-space: pre-wrap;
  }

  small {
    font-size: 1rem;
    color: #757575;
  }

  h1, h2, h3, h4, h5, h6 {
    margin: 1rem 0;
  }

  h1, h2, h3, h4 {
    font-weight: 400;
    letter-spacing: .2px;
    line-height: 1.4;
  }

  h5, h6 {
    font-weight: 700;
    letter-spacing: .4px;
  }

  h1, h2 {
    font-size: 3rem;
  }

  h3 {
    font-size: 1.75rem;
  }

  h4 {
    font-size: 1.5rem;
  }

  h5 {
    font-size: 1.25rem;
  }

  h6 {
    font-size: 1.125rem;
  }
`

export default GlobalStyle
