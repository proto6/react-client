import { useState } from 'react'
import useSWR, { mutate } from 'swr'
import api from '../api'

export function useUser(id) {
  const { data, error } = useSWR(id ? `/users/${id}` : null)

  return {
    user: data,
    isLoading: !error && !data,
    error,
  }
}

export function useArticle(id) {
  const { data, error } = useSWR(id ? `/articles/${id}` : null)
  const [submitError, setSubmitError] = useState(false)

  async function put(id, values, callback) {
    try {
      const response = await api.put(`/articles/${id}`, values)
      mutate(`/articles/${response.id}`, response, false)
      mutate('/articles', async (articles) => {
        const filteredArticles = articles.filter(({ id }) => id !== response.id)
        return [...filteredArticles, response]
      })
      callback && callback(response)
    } catch (error) {
      setSubmitError(error)
    }
  }

  async function post(values, callback) {
    try {
      const response = await api.post('/articles', values)
      mutate(`/articles/${response.id}`, response, false)
      mutate('/articles', async (articles) => {
        const filteredArticles = articles.filter(({ id }) => id !== response.id)
        return [...filteredArticles, response]
      })
      callback && callback(response)
    } catch (error) {
      setSubmitError(error)
    }
  }

  async function remove(id, callback) {
    try {
      await api.delete(`/articles/${id}`)
      mutate(`/articles/${id}`, null)
      mutate('/articles', async (articles) => {
        const filteredArticles = articles.filter(
          ({ id: oldId }) => oldId !== id
        )
        return [...filteredArticles]
      })
      callback && callback()
    } catch (error) {
      setSubmitError(error)
    }
  }

  return {
    article: data,
    isLoading: !error && !data,
    error,
    submitError,
    put,
    post,
    remove,
  }
}

export function useArticles() {
  const { data, error } = useSWR(`/articles`)

  if (data) {
    data.forEach((article) => {
      mutate(`/articles/${article.id}`, article, false)
    })
  }

  return {
    articles: data,
    isLoading: !error && !data,
    error,
  }
}
