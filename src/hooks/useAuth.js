import { createContext, useContext, useEffect, useState } from 'react'
import api from '../api'

const authContext = createContext()

function useProvideAuth() {
  const [user, setUser] = useState(null)

  useEffect(() => {
    async function fetchSession() {
      return setUser(await api.get('/session'))
    }
    fetchSession()
  }, [])

  return {
    user,
    setUser,
  }
}

export function ProvideAuth({ children }) {
  const auth = useProvideAuth()
  return <authContext.Provider value={auth}>{children}</authContext.Provider>
}

function useAuth() {
  const { user, setUser } = useContext(authContext)
  const [error, setError] = useState(false)

  async function login(values, callback) {
    try {
      const response = await api.post('/session/login', values)
      setUser(response)
      callback && callback(response)
    } catch (error) {
      setUser(false)
      setError(error)
    }
  }

  async function signup(values, callback) {
    try {
      const response = await api.post('/users', values)
      setUser(response)
      callback && callback(response)
    } catch (error) {
      setError(error)
    }
  }

  async function logout(callback) {
    await api.get('/session/logout')
    setUser(false)
    callback && callback()
  }

  return {
    user,
    error,
    login,
    signup,
    logout,
  }
}

export default useAuth
