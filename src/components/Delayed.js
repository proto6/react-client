import { useEffect, useState } from 'react'

function Delayed({ children, wait = 500 }) {
  const [hidden, setHidden] = useState(true)

  useEffect(() => {
    let ignore = false

    const id = setTimeout(() => {
      if (!ignore) {
        setHidden(false)
      }
    }, wait)

    return () => {
      ignore = true
      clearTimeout(id)
    }
  }, [wait])

  if (hidden) return null

  return children
}

export default Delayed
