import styled, { css } from 'styled-components'

const Row = styled.div`
  margin: 1.5rem 0;

  &:last-child {
    margin-bottom: 0;
  }

  ${(props) =>
    props.bg &&
    css`
      padding: 1rem;
      background: whitesmoke;
      border-radius: 0.5rem;
    `}
`

export default Row
