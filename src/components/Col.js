import styled from 'styled-components'

const Col = styled.span`
  display: inline-block;

  &:not(:first-child) {
    margin-left: ${(props) => (props.small ? '0.75rem' : '1.5rem')};
  }
`

export default Col
