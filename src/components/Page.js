import styled from 'styled-components'

const Page = styled.div`
  flex: 0 1 60rem;
  padding: 0 3rem;
  box-sizing: border-box;

  ${(props) => props.thin && 'flex-basis: 30rem'};
`

export default Page
