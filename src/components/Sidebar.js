import styled from 'styled-components'

const Sidebar = styled.div`
  flex: 0 0 30rem;
  padding: 0 3rem;
  box-sizing: border-box;
`

export default Sidebar
