import styled, { css } from 'styled-components'
import { useField } from 'formik'

const StyledInput = styled.input`
  margin: 0.25rem 0 1rem;

  ${(props) =>
    props.error &&
    css`
      padding: calc(0.5rem - 2px) calc(1rem - 1px);
      border: 2px solid orangered;
    `};
`

function TextInput({ label, innerRef, submitCount, ...props }) {
  const [field, meta] = useField(props)

  return (
    <>
      <label htmlFor={props.id || props.name}>{label}</label>
      <StyledInput
        id={props.id || props.name}
        ref={innerRef}
        error={submitCount && meta.error}
        {...props}
        {...field}
      />
    </>
  )
}

export default TextInput
