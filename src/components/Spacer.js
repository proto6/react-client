import styled from 'styled-components'

const Spacer = styled.span`
  flex: 1 1 auto;
`

export default Spacer
