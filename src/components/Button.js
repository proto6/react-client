import styled, { css } from 'styled-components'

const Button = styled.button`
  display: ${(props) => (props.wide ? 'block' : 'inline-block')};
  box-sizing: border-box;
  text-decoration: none;
  text-align: center;
  background: none;
  color: rebeccapurple;
  border-radius: 0.5rem;
  border: 2px solid rebeccapurple;
  line-height: calc(2.75rem - 4px);
  padding: 0 1.5rem;
  font-size: 1.25rem;
  outline: none;

  &:hover,
  &:active,
  &:focus,
  &:focus-visible {
    background: tomato;
    border-color: tomato;
    color: white;
  }

  &[disabled] {
    color: silver;
    border-color: silver;
    pointer-events: none;
  }

  ${(props) =>
    props.primary &&
    css`
      background: rebeccapurple;
      color: white;

      &[disabled] {
        background: silver;
        color: #757575;
      }
    `}

  ${(props) =>
    props.small &&
    css`
      padding: 0 1rem;
      line-height: 2rem;
      font-size: 0.875rem;
      font-weight: 700;
      border: none;
      border-radius: 6px;
    `}
`

export const Decorator = styled.span`
  display: inline-block;
  ${(prop) => (prop.left ? 'margin-right: 0.5rem' : 'margin-left: 0.5rem')};
`

export default Button
