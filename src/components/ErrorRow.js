import styled from 'styled-components'
import Row from './Row'

const StyledRow = styled(Row)`
  background-color: rgba(255, 69, 0, 0.25);
  padding: 1rem;
  border-radius: 0.5rem;
`

function ErrorRow({ name, message, children }) {
  return (
    <StyledRow>
      <strong>
        {name}: {message}
      </strong>
      {children}
    </StyledRow>
  )
}

export default ErrorRow
