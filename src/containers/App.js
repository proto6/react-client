import { BrowserRouter, Route, Switch } from 'react-router-dom'
import GlobalStyle from '../global-styles'
import Header from './App/Header'
import Main from './App/Main'
import Navigation from './App/Navigation'
import { ProvideAuth } from '../hooks/useAuth'
import { routes } from '../routes'
import { SWRConfig } from 'swr'
import api from '../api'
import PrivateRoute from './Routes/PrivateRoute'

function App() {
  return (
    <>
      <GlobalStyle />
      <SWRConfig
        value={{
          revalidateIfStale: false,
          revalidateOnFocus: false,
          revalidateOnReconnect: false,
          fetcher: api.get,
        }}
      >
        <ProvideAuth>
          <BrowserRouter>
            <Header />
            <hr style={{ margin: 0 }} />
            <Navigation />
            <Main>
              <Switch>
                {routes.map(({ path, Component, requiresAuth }) =>
                  requiresAuth ? (
                    <PrivateRoute key={path} path={path} exact>
                      <Component />
                    </PrivateRoute>
                  ) : (
                    <Route key={path} path={path} exact>
                      <Component />
                    </Route>
                  )
                )}
              </Switch>
            </Main>
          </BrowserRouter>
        </ProvideAuth>
      </SWRConfig>
    </>
  )
}

export default App
