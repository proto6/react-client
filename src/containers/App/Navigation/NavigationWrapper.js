import styled from 'styled-components'

const NavigationWrapper = styled.nav`
  display: flex;
  align-items: center;
  padding: 0 3rem;
  font-size: 1rem;
  height: 4rem;
  max-width: 90rem;
  margin: 0 auto;
`

export default NavigationWrapper
