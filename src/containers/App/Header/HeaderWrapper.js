import styled from 'styled-components'

const HeaderWrapper = styled.header`
  display: flex;
  align-items: center;
  height: 4rem;
  max-width: 90rem;
  padding: 0 3rem;
  margin: 0 auto;
`

export default HeaderWrapper
