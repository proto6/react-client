import styled from 'styled-components'

const Logo = styled.h3`
  font-weight: 700;
  text-transform: uppercase;

  a:hover,
  a:focus {
    text-decoration: none;
  }
`

export default Logo
