import { Link, useLocation } from 'react-router-dom'
import HeaderWrapper from './Header/HeaderWrapper'
import Spacer from '../../components/Spacer'
import Col from '../../components/Col'
import useAuth from '../../hooks/useAuth'
import Logo from './Header/Logo'
import Button from '../../components/Button'

function Header() {
  const auth = useAuth()
  const { pathname } = useLocation()

  return (
    <HeaderWrapper>
      <Logo>
        {pathname === '/' ? (
          <span>React Client</span>
        ) : (
          <Link to="/">React Client</Link>
        )}
      </Logo>
      <Spacer />
      {auth.user !== null && (
        <>
          <Col>
            {auth.user && (
              <small>
                {`Logged in as `}
                <Link to={`/user/${auth.user.id}`}>
                  {auth.user.fname} {auth.user.lname}
                </Link>
                .
              </small>
            )}
          </Col>
          {auth.user && (
            <Col small="true">
              <Button small="true" onClick={() => auth.logout()}>
                Log out
              </Button>
            </Col>
          )}
          {!auth.user && !['/login', '/signup'].includes(pathname) && (
            <>
              <Col small="true">
                <Button small="true" as={Link} to="/login">
                  Log in
                </Button>
              </Col>
              <Col small="true">
                <Button small="true" primary="true" as={Link} to="/signup">
                  Sign up
                </Button>
              </Col>
            </>
          )}
        </>
      )}
    </HeaderWrapper>
  )
}

export default Header
