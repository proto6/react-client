import { useHistory, useRouteMatch } from 'react-router-dom'
import Button, { Decorator } from '../../components/Button'
import NavigationWrapper from './Navigation/NavigationWrapper'

function Navigation() {
  const match = useRouteMatch('/')
  const history = useHistory()

  function back(e) {
    e.stopPropagation()
    history.goBack()
  }

  return (
    <NavigationWrapper>
      {!match.isExact ? (
        <Button small="true" onClick={back}>
          <Decorator left="true">❮</Decorator>
          Back
        </Button>
      ) : null}
    </NavigationWrapper>
  )
}

export default Navigation
