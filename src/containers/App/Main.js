import styled from 'styled-components'

const Main = styled.main`
  max-width: 80rem;
  padding: 0 1.5rem 10rem;
  margin: 0 auto;
`

export default Main
