import Article from './pages/Article'
import Start from './pages/Start'
import User from './pages/User'
import Signup from './pages/Signup'
import Login from './pages/Login'
import ArticleNew from './pages/Article/ArticleNew'
import ArticleEdit from './pages/Article/ArticleEdit'
import ErrorPage from './pages/ErrorPage'

export const routes = [
  {
    path: '/',
    Component: Start,
  },
  {
    path: '/login',
    Component: Login,
  },
  {
    path: '/signup',
    Component: Signup,
  },
  {
    path: '/article/new',
    requiresAuth: true,
    Component: ArticleNew,
  },
  {
    path: '/article/:id',
    Component: Article,
  },
  {
    path: '/article/:id/edit',
    requiresAuth: true,
    Component: ArticleEdit,
  },
  {
    path: '/user/:id',
    Component: User,
  },
  {
    path: '*',
    Component: () => <ErrorPage message="Not Found" />,
  },
]
