class FetchError extends Error {
  constructor(response, message, ...params) {
    super(...params)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, FetchError)
    }
    this.name = 'Fetch Error'
    this.status = response.status
    this.statusText = response.statusText
    this.message = message || response.statusText
  }
}

function formatter(response) {
  if (!response.ok) {
    throw new FetchError(response)
  }
  const contentType = response.headers.get('Content-Type')
  if (contentType && contentType.includes('application/json')) {
    return response.json()
  } else {
    return false
  }
}

const api = {
  get: async function (endpoint) {
    return formatter(
      await fetch(`http://localhost:7000${endpoint}`, {
        method: 'GET',
        credentials: 'include',
      })
    )
  },

  post: async function (endpoint, obj) {
    return formatter(
      await fetch(`http://localhost:7000${endpoint}`, {
        method: 'POST',
        credentials: 'include',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(obj),
      })
    )
  },

  put: async function (endpoint, obj) {
    return formatter(
      await fetch(`http://localhost:7000${endpoint}`, {
        method: 'PUT',
        credentials: 'include',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(obj),
      })
    )
  },

  delete: async function (endpoint) {
    return formatter(
      await fetch(`http://localhost:7000${endpoint}`, {
        method: 'DELETE',
        credentials: 'include',
      })
    )
  },
}

export default api
