import { Form, Formik } from 'formik'
import Button from '../../components/Button'
import ErrorRow from '../../components/ErrorRow'
import Row from '../../components/Row'
import TextInput from '../../components/TextInput'

function ArticleForm({ initialValues, onSubmit }) {
  return (
    <Formik
      initialValues={initialValues}
      validate={(values) => {
        const errors = {}
        if (!values.title) {
          errors.title = 'is required'
        }
        if (!values.text) {
          errors.text = 'is required'
        }
        return errors
      }}
      onSubmit={onSubmit}
    >
      {({ isSubmitting, submitCount, isValid, errors }) => (
        <Form>
          <Row>
            <TextInput
              label="Title"
              name="title"
              type="text"
              submitCount={submitCount}
            />
            <TextInput
              label="Text"
              name="text"
              as="textarea"
              submitCount={submitCount}
            />
          </Row>
          <Row>
            <Button primary="true" type="submit" disabled={isSubmitting}>
              Submit
            </Button>
          </Row>
          {submitCount && !isValid ? (
            <ErrorRow
              name="Validation Error"
              message="One or more fields failed to validate"
            >
              <ul>
                {errors.title && <li>Title {errors.title}</li>}
                {errors.text && <li>Text {errors.text}</li>}
              </ul>
            </ErrorRow>
          ) : null}
        </Form>
      )}
    </Formik>
  )
}

export default ArticleForm
