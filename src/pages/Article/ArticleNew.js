import { useHistory } from 'react-router-dom'
import ErrorRow from '../../components/ErrorRow'
import Flex from '../../components/Flex'
import Page from '../../components/Page'
import { useArticle } from '../../hooks/swrHooks'
import ArticleForm from './ArticleForm'

function ArticleNew() {
  const { submitError, post } = useArticle(false)
  const history = useHistory()

  function handleSubmit(values, { setSubmitting }) {
    setTimeout(async () => {
      await post(values, ({ id }) => {
        history.replace(`/article/${id}`)
      })
      setSubmitting(false)
    }, 400)
  }

  const initialValues = {
    title: '',
    text: '',
  }

  return (
    <Flex>
      <Page>
        <h1>New Article</h1>
        <ArticleForm initialValues={initialValues} onSubmit={handleSubmit} />
        {submitError && <ErrorRow {...submitError} />}
      </Page>
    </Flex>
  )
}

export default ArticleNew
