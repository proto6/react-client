import { useHistory, useParams } from 'react-router-dom'
import Delayed from '../../components/Delayed'
import ErrorRow from '../../components/ErrorRow'
import Flex from '../../components/Flex'
import Page from '../../components/Page'
import { useArticle } from '../../hooks/swrHooks'
import useAuth from '../../hooks/useAuth'
import ErrorPage from '../ErrorPage'
import ArticleForm from './ArticleForm'

function ArticleEdit() {
  const auth = useAuth()
  const { id } = useParams()
  const history = useHistory()
  const { isLoading, error, article, submitError, put } = useArticle(id)

  function handleSubmit(values, { setSubmitting }) {
    setTimeout(async () => {
      await put(id, values, () => {
        history.goBack()
      })
      setSubmitting(false)
    }, 400)
  }

  if (error) return <ErrorPage {...error} />
  if (isLoading) return <Delayed>Loading...</Delayed>
  if (auth.user.id !== article.userId) return <ErrorPage message="Forbidden" />

  const initialValues = {
    title: article.title,
    text: article.text,
  }

  return (
    <Flex>
      <Page>
        <h1>Edit Article</h1>
        <ArticleForm initialValues={initialValues} onSubmit={handleSubmit} />
        {submitError && <ErrorRow {...submitError} />}
      </Page>
    </Flex>
  )
}

export default ArticleEdit
