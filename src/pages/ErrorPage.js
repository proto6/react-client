import Flex from '../components/Flex'
import Page from '../components/Page'

function ErrorPage({ message }) {
  return (
    <Flex>
      <Page>
        <h1>{message}</h1>
      </Page>
    </Flex>
  )
}

export default ErrorPage
