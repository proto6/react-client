import formatDistanceToNow from 'date-fns/formatDistanceToNow'
import { Link, useHistory, useParams, useRouteMatch } from 'react-router-dom'
import Delayed from '../components/Delayed'
import Flex from '../components/Flex'
import Page from '../components/Page'
import Spacer from '../components/Spacer'
import Col from '../components/Col'
import Row from '../components/Row'
import Button from '../components/Button'
import { useArticle, useUser } from '../hooks/swrHooks'
import ErrorPage from './ErrorPage'

function Article() {
  const { id } = useParams()
  const { url } = useRouteMatch()
  const history = useHistory()
  const { article, error: articleError, remove } = useArticle(id)
  const { user, error: userError } = useUser(article && article.userId)

  function handleDelete() {
    if (window.confirm('Do you really want to delete this article?')) {
      remove(id, () => {
        history.goBack()
      })
    }
  }

  if (articleError) return <ErrorPage {...articleError} />
  if (userError) return <ErrorPage {...userError} />
  if (!user) return <Delayed>Loading...</Delayed>

  const createdAtStr = formatDistanceToNow(new Date(article.createdAt), {
    addSuffix: true,
  })
  const updatedAtStr = article.updatedAt
    ? formatDistanceToNow(new Date(article.updatedAt), {
        addSuffix: true,
      })
    : null

  return (
    <Flex>
      <Page>
        <article>
          <h1>{article.title}</h1>
          <small>
            {'Written by '}
            <Link to={`/user/${article.userId}`}>
              {`${user.fname} ${user.lname}`}
            </Link>
            {` ${createdAtStr}${
              article.updatedAt ? `, edited ${updatedAtStr}` : ''
            }`}
          </small>
          <p>{article.text}</p>
        </article>
        <Row bg="true">
          <Col small="true">
            <Button small="true" as={Link} to={`${url}/edit`}>
              Edit
            </Button>
          </Col>
          <Col small="true">
            <Button small="true" onClick={handleDelete}>
              Delete
            </Button>
          </Col>
        </Row>
        <Row bg="true">
          <Flex align="center">
            <small>Log in or sign up to leave a comment</small>
            <Spacer />
            <Col small="true">
              <Button small="true" as={Link} to="/login">
                Log in
              </Button>
            </Col>
            <Col small="true">
              <Button small="true" primary="true" as={Link} to="/signup">
                Sign up
              </Button>
            </Col>
          </Flex>
        </Row>
      </Page>
    </Flex>
  )
}

export default Article
