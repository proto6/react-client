import Delayed from '../../components/Delayed'
import { useArticles } from '../../hooks/swrHooks'
import ArticleItem from './ArticleItem'

function ArticleList() {
  const { isLoading, error, articles } = useArticles()

  if (isLoading) return <Delayed>Loading...</Delayed>
  if (error) return 'Error!'

  return (
    <ol>
      {articles.map((article, index) => (
        <ArticleItem key={article.id} article={article} index={index} />
      ))}
    </ol>
  )
}

export default ArticleList
