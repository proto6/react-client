import { Link } from 'react-router-dom'
import { formatDistanceToNow } from 'date-fns'
import Delayed from '../../components/Delayed'
import { useUser } from '../../hooks/swrHooks'

function ArticleItem({ article, index }) {
  const { isLoading, error, user } = useUser(article.userId)

  if (error) return 'Error'
  if (isLoading) return <Delayed>Loading...</Delayed>

  const createdAtStr = formatDistanceToNow(new Date(article.createdAt), {
    addSuffix: true,
  })

  return (
    <>
      {index ? <hr /> : null}
      <li>
        <article>
          <h4 style={{ margin: 0 }}>
            <Link to={`/article/${article.id}`}>{article.title}</Link>
          </h4>
          <small>
            {'Written by '}
            <Link to={`/user/${article.userId}`}>
              {`${user.fname} ${user.lname}`}
            </Link>
            {` ${createdAtStr}`}
          </small>
        </article>
      </li>
    </>
  )
}

export default ArticleItem
