import Flex from '../components/Flex'
import Page from '../components/Page'

function User() {
  return (
    <Flex>
      <Page>
        <h1>User</h1>
        <div>[user info / form]</div>
      </Page>
    </Flex>
  )
}

export default User
