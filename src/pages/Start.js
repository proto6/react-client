import { Link } from 'react-router-dom'
import ArticleList from './Start/ArticleList'
import Flex from '../components/Flex'
import Page from '../components/Page'
import Sidebar from '../components/Sidebar'
import useAuth from '../hooks/useAuth'
import Row from '../components/Row'
import Button from '../components/Button'

function Start() {
  const auth = useAuth()

  return (
    <Flex>
      <Page>
        <ArticleList />
      </Page>
      <Sidebar>
        <h1>Articles</h1>
        <p>
          Start page Lorem, ipsum dolor sit amet consectetur adipisicing elit.
          Dignissimos sapiente, architecto dolorum voluptatibus iusto eveniet
          similique necessitatibus. Non, dolore harum. Temporibus, quia
          reiciendis iusto cum laudantium nam ad natus alias!
        </p>
        {auth.user && (
          <Row>
            <Button wide="true" as={Link} to="/article/new">
              New article
            </Button>
          </Row>
        )}
      </Sidebar>
    </Flex>
  )
}

export default Start
