import { useEffect, useRef } from 'react'
import { Form, Formik } from 'formik'
import { Link, useHistory, useLocation } from 'react-router-dom'
import Button from '../components/Button'
import Flex from '../components/Flex'
import Row from '../components/Row'
import Page from '../components/Page'
import TextInput from '../components/TextInput'
import useAuth from '../hooks/useAuth'
import ErrorRow from '../components/ErrorRow'

function Signup() {
  const auth = useAuth()
  const location = useLocation()
  const history = useHistory()
  const inputEl = useRef(null)
  const { from } = location.state || {}

  useEffect(() => {
    inputEl.current.focus()
  }, [])

  async function handleSubmit(values, { setSubmitting }) {
    await auth.signup(values, () => {
      if (from) {
        history.replace(from)
      } else {
        history.goBack()
      }
    })
    setSubmitting(false)
  }

  return (
    <Flex>
      <Page thin="true">
        <h1>Sign up</h1>
        <Formik
          initialValues={{ username: '', fname: '', lname: '' }}
          validate={(values) => {
            const errors = {}
            if (!values.username) {
              errors.username = 'is required'
            }
            if (!values.fname) {
              errors.fname = 'is required'
            }
            if (!values.lname) {
              errors.lname = 'is required'
            }
            return errors
          }}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting, submitCount, isValid, errors }) => (
            <Form>
              <Row>
                <TextInput
                  innerRef={inputEl}
                  label="Username"
                  name="username"
                  type="text"
                  submitCount={submitCount}
                />
                <TextInput
                  label="First name"
                  name="fname"
                  type="text"
                  submitCount={submitCount}
                />
                <TextInput
                  label="Last name"
                  name="lname"
                  type="text"
                  submitCount={submitCount}
                />
              </Row>
              <Row>
                <Flex justify="space-between" align="center">
                  <Button primary="true" type="submit" disabled={isSubmitting}>
                    Submit
                  </Button>
                  <span>
                    <Link
                      to={{
                        pathname: '/login',
                        state: location.state,
                      }}
                      replace
                    >
                      Already a member?
                    </Link>
                  </span>
                </Flex>
              </Row>
              {submitCount && !isValid ? (
                <ErrorRow
                  name="Validation Error"
                  message="One or more fields failed to validate"
                >
                  <ul>
                    {errors.username && <li>Username {errors.username}</li>}
                    {errors.fname && <li>First name {errors.fname}</li>}
                    {errors.lname && <li>Last name {errors.lname}</li>}
                  </ul>
                </ErrorRow>
              ) : null}
            </Form>
          )}
        </Formik>
        {auth.error && <ErrorRow {...auth.error} />}
      </Page>
    </Flex>
  )
}

export default Signup
